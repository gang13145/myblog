module.exports = {
    title: 'yaner~blog',
    description: 'Just playing around',
    //浏览器标签页图标
    head: [
        ["link", {rel: "icon", href: "/logo.jpeg"}]
    ],
    themeConfig: {
        nav: [
            {text: 'Home', link: '/'},
            {text: '架构', link: '/architecture/'},
            {text: '微服务', link: '/microservices/'},
            {text: '后端',
                items: [
                    { text: 'java', link: '/backend/java/' },
                    { text: 'netty', link: '/backend/netty/' },
                    { text: 'spring', link: '/backend/spring/' },
                    { text: 'spring cloud', link: '/backend/spring cloud/' }]
            },
            {text: '前端', items: [
                    { text: 'react', link: '/frontend/react/' },
                    { text: 'vue', link: '/frontend/vue/' }]
            },
        ],
        sidebar: {
            "/backend/java/": ['00 java基础', '01 java反射', '02 java流', '03 java高级', '04 jvm'],
            "/architecture/": ['00 DDD', '01 设计模式'],
            "/microservices/": ['手写rpc']
        }
    }
}